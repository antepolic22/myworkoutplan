import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-make-aplan',
  templateUrl: './make-aplan.component.html',
  styleUrls: ['./make-aplan.component.css']
})
export class MakeAPlanComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  selectedItems: string[] = [];
  selectedItemsCount = 0;

  excersizeGroups = [
    {
      id: 1,
      name: 'Hammer Strength Iso Lateral Shoulder Press',
      img: 'http://lorempixel.com/100/100'
    },
    {
      id: 2,
      name: '2jsd oiwdfoi sief iswefhi sdwejihd',
      img: 'http://lorempixel.com/101/101'
    },
    {
      id: 3,
      name: '3',
      img: 'http://lorempixel.com/102/102'
    },
    {
      id: 4,
      name: '444 55sd sdsdere dfdf fghfghfgdrwdf dfds',
      img: 'http://lorempixel.com/103/103'
    },
    {
      id: 5,
      name: '5',
      img: 'http://lorempixel.com/104/104'
    },
    {
      id: 6,
      name: '6',
      img: 'http://lorempixel.com/105/105'
    },
    {
      id: 7,
      name: '7',
      img: 'http://lorempixel.com/106/106'
    },
    {
      id: 8,
      name: '8',
      img: 'http://lorempixel.com/107/107'
    },
    {
      id: 9,
      name: '9',
      img: 'http://lorempixel.com/108/108'
    },
    {
      id: 10,
      name: '10',
      img: 'http://lorempixel.com/109/109'
    },
    {
      id: 11,
      name: '11',
      img: 'http://lorempixel.com/110/110'
    },
    {
      id: 12,
      name: '12',
      img: 'http://lorempixel.com/111/111'
    },
    {
      id: 13,
      name: '13',
      img: 'http://lorempixel.com/112/112'
    },
  ];

  changeSelectedItem(e) {
    if (this.selectedItems.length > 0) {
      if (this.selectedItems.find(obj => obj === e.target.id) && e.target.checked === false) {
        const index: number = this.selectedItems.indexOf(e.target.id);
        if (index !== -1) {
          this.selectedItems.splice(index, 1);
        }
      }
      if (this.selectedItems.find(obj => obj !== e.target.id) && e.target.checked === true) {
        this.selectedItems.push(e.target.id);
      }
    } else {
      if (e.target.checked === true) {
        this.selectedItems.push(e.target.id);
      }
    }
    this.selectedItemsCount = this.selectedItems.length;
    if (this.selectedItemsCount === 0) {
      document.getElementById('btnSelectGroups').style.display = 'none';
    } else {
      document.getElementById('btnSelectGroups').style.display = 'block';
    }
  }

  // When the user scrolls down 20px from the top of the document, show the button
  scrollFunctionToTop() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
      document.getElementById('btnToTop').style.display = 'block';
    } else {
      document.getElementById('btnToTop').style.display = 'none';
    }
  }

  // When the user clicks on the button, scroll to the top of the document
  topFunction() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }

  selectGroups() {
    let groups = '';
    this.selectedItems.forEach(element => {
      if (groups.length === 0) {
        groups = element + ' ;';
      } else {
        groups = groups + element + ' ;';
      }
    });
    alert('You select following groups: ' + groups);
  }

  @HostListener('window:scroll', []) onWindowScroll() {
    this.scrollFunctionToTop();
  }

}
